#!/usr/bin/env bash

#edited: 14/11/2012 13:08 furushchev

#OpenCV 2.4.3 Full Installation for Ubuntu(Debian)
#Checked on Ubuntu 12.04LTE x86-64

if [ ! -f /usr/bin/aptitude ]; then
    sudo apt-get -y install aptitude
fi

sudo aptitude -y install build-essential

cd /tmp; sudo apt-get source opencv
sudo aptitude -y build-dep opencv 

sudo aptitude -y install libjpeg-dev
sudo aptitude -y install libopenjpeg-dev
sudo aptitude -y install jasper
sudo aptitude -y install libjasper-dev libjasper-runtime
sudo aptitude -y install libpng12-dev
sudo aptitude -y install libpng++-dev libpng3
sudo aptitude -y install libpnglite-dev libpngwriter0-dev libpngwriter0c2
sudo aptitude -y install libtiff-dev libtiff-tools pngtools
sudo aptitude -y install zlib1g-dev zlib1g-dbg
sudo aptitude -y install v4l2ucp

sudo aptitude -y install python
sudo aptitude -y install autoconf
sudo aptitude -y install libtbb2 libtbb-dev
sudo aptitude -y install libeigen2-dev
sudo aptitude -y install cmake
sudo aptitude -y install openexr
sudo aptitude -y install gstreamer-plugins-*
sudo aptitude -y install freeglut3-dev
sudo aptitude -y install libglui-dev
sudo aptitude -y install libavc1394-dev libdc1394-22-dev libdc1394-utils
# ビデオ関係のパッケージ 
sudo aptitude -y install libxine-dev
sudo aptitude -y install libxvidcore-dev 
sudo aptitude -y install libva-dev
sudo aptitude -y install libssl-dev
sudo aptitude -y install libv4l-dev
sudo aptitude -y install libvo-aacenc-dev
sudo aptitude -y install libvo-amrwbenc-dev 
sudo aptitude -y install libvorbis-dev 
sudo aptitude -y install libvpx-dev

cd /tmp
if [ ! -f OpenCV-2.4.3.tar.bz2 ]; then 
    wget http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/2.4.3/OpenCV-2.4.3.tar.bz2
fi 

sudo rm -rf OpenCV-2.4.3
tar -xvjof /tmp/OpenCV-2.4.3.tar.bz2
cd OpenCV-2.4.3
cmake -DBUILD_DOCS=ON -DBUILD_EXAMPLES=ON -DCMAKE_BUILD_TYPE=RELEASE -DWITH_TBB=ON -DINSTALL_C_EXAMPLES=ON -DWITH_CUDA=OFF -DWITH_OPENNI=ON -DWITH_UNICAP=ON -DWITH_V4L=ON -DWITH_XINE=ON  .
make -j
sudo make install
sudo ldconfig
echo "インストール＼(^o^)／ｵﾜﾀ"

