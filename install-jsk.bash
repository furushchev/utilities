#!/bin/bash

USERNAME=hoge

sudo apt-get install -yV subversion ssh aptitude 
SVN_SSH="ssh -l $USERNAME" svn co -N svn+ssh://aries.jsk.t.u-tokyo.ac.jp/home/jsk/svn/trunk prog

{
echo 'alias sudo="sudo -E"'
echo 'export SVN_SSH="ssh -l $USERNAME"'
echo "export http_proxy='http://jenkins.jsk.imi.i.u-tokyo.ac.jp:3142'"
echo "export ftp_proxy='http://jenkins.jsk.imi.i.u-tokyo.ac.jp:3142'"
} >> } ~/.bashrc

cd prog
svn up scripts
cd scripts
wget https://jsk-ros-pkg.svn.sourceforge.net/svnroot/jsk-ros-pkg/trunk/jsk.rosbuild -O /tmp/jsk.rosbuild
yes p | bash /tmp/jsk.rosbuild fuerte
cd ~/ros/fuerte
source setup.bash
rosws merge http://rtm-ros-robotics.googlecode.com/svn/trunk/agentsystem_ros_tutorials/rtm-ros-robotics.rosinstall
cd ~/prog/scripts/
svn up
cd -
rosws merge ~/prog/scripts/jsk_fuerte.rosinstall
cd ~/ros/fuerte
rosws update
